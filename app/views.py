from django.shortcuts import render
from django.http import HttpResponse

# Lista de Peliculas
lista = [
    {'id': 0, 'nombre': 'Pulp Fiction', 'year': 1998},
    {'id': 1, 'nombre': 'Your Name', 'year': 2015},
    {'id': 2, 'nombre': 'Django Unchained', 'year': 2013},
    {'id': 3, 'nombre': 'A Silent Voice', 'year': 2014},
]


def index(request):
    return render(request, 'app/index.html')


def peliculas(request):
    contexto = {
        'lista_peliculas': lista,
        'year': 2020,
        'tienda': 'Blockbuster',

    }
    return render(request, 'app/peliculas.html', contexto)


def pelicula(request, id):
    contexto = {}

    # Verifica que el ID este dentro de la lista
    if id < len(lista):
        # Crea una nueva propiedad en el contexto
        contexto['pelicula'] = lista[id]

# Muestra el Template con la información del contexto
    return render(request, 'app/pelicula.html', contexto)


def categorias(request):
    return render(request, 'app/categorias.html')
