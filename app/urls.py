from django.urls import path
from . import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),
    path('peliculas/', views.peliculas, name='peliculas'),
    path('peliculas/<int:id>/', views.pelicula, name='pelicula'),
    path('categorias/', views.categorias, name='categorias'),

]
